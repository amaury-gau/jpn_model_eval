Lecture du corpus GSD : 0.33 secondes 
Entraînement du corpus GSD sur ja_core_news_sm: 0.83 secondes
------ Résultats du modèle Corpus GSD -ja_core_news_sm ------


Précision du pos tagging : 0.89            
Précision sur OOV : 0.67



Entraînement du corpus GSD sur ja_gsd-1.0: 0.39 secondes
------ Résultats du modèle Corpus GSD -ja_gsd-1.0 ------


Précision du pos tagging : 0.89            
Précision sur OOV : 0.68



Temps total : 151.3 secondes
