import os
import glob
import nagisa

def write_file_conll(fn_in, fn_out):
    with open(fn_in, "r") as f:
        data = []
        words = []
        postags = []
        for line in f:
            line = line.strip()

            if len(line) > 0:
                prefix = line[0]
                if prefix != "#":
                    tokens = line.split("\t")
                    word = tokens[1]
                    postag = tokens[4]
                    words.append(word)
                    postags.append(postag)

            else:
                if (len(words) > 0) and (len(postags) > 0):
                    data.append([words, postags])
                    words = []
                    postags = []

    with open(fn_out, "w") as f:
        for words, postags in data:
            for word, postag in zip(words, postags):
                f.write("\t".join([word, postag])+"\n")
            f.write("EOS\n")


def write_file_knp(type_corpus: str, fn_out: str):
    if os.path.exists(fn_out):
        os.remove(fn_out)
    with open('../data/KWDLC/'+type_corpus+'.id', 'r') as fin:
        list_file_name = fin.readlines()
    list_file_name = [file.strip()+'.knp' for file in list_file_name]

    for file in glob.glob("../data/KWDLC/knp/*/*"):
        filename = os.path.basename(file)
        if filename in list_file_name:
            with open(file, "r") as f:
                data = []
                words = []
                postags = []
                for line in f:
                    line = line.strip()

                    if len(line) > 0:
                        prefix = line[0]
                        if prefix != "#" and prefix != "*" and prefix != "+" and line != "EOS":
                            tokens = line.split()
                            word = tokens[0]
                            postag = tokens[3]
                            words.append(word)
                            postags.append(postag)

                        elif line == "EOS":
                            if (len(words) > 0) and (len(postags) > 0):
                                data.append([words, postags])
                                words = []
                                postags = []
            with open(fn_out, "a") as f:
                for words, postags in data:
                    for word, postag in zip(words, postags):
                        f.write("\t".join([word, postag])+"\n")
                    f.write("EOS\n")


if __name__ == "__main__":
    # files
    # fn_in_train = "../data/GSD/ja_gsd-ud-train.conllu"
    # fn_in_dev = "../data/GSD/ja_gsd-ud-dev.conllu"
    # fn_in_test = "../data/GSD/ja_gsd-ud-test.conllu"

    # fn_out_train = "../data/GSD_nagisa/ja_gsd_ud_jptag.train"
    # fn_out_dev = "../data/GSD_nagisa/ja_gsd_ud_jptag.dev"
    # fn_out_test = "../data/GSD_nagisa/ja_gsd_ud_jptag.test"

    # fn_out_model = "../models/nagisa/ja_gsd_ud_jptag"

    # # write files for nagisa
    # write_file(fn_in_train, fn_out_train)
    # write_file(fn_in_dev, fn_out_dev)
    # write_file(fn_in_test, fn_out_test)

    # # start training
    # nagisa.fit(train_file=fn_out_train, dev_file=fn_out_dev,
    #            test_file=fn_out_test, model_name=fn_out_model)

    # files
    fn_out_train = "../data/KWDLC_nagisa/ja_kwdlc_knp.train"
    fn_out_dev = "../data/KWDLC_nagisa/ja_kwdlc_knp.dev"
    fn_out_test = "../data/KWDLC_nagisa/ja_kwdlc_knp.test"

    fn_out_model = "../models/nagisa/ja_kwdlc_knp"

    # write files for nagisa
    write_file_knp("train", fn_out_train)
    write_file_knp("dev", fn_out_dev)
    write_file_knp("test", fn_out_test)

    # start training
    nagisa.fit(train_file=fn_out_train, dev_file=fn_out_dev,
               test_file=fn_out_test, model_name=fn_out_model)
