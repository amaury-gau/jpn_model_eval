"""
Ce script permet de tester l'annotation automatique en pos à partir de fichiers conll
"""

import argparse, sys
from typing import Tuple, Optional

import spacy
from sklearn.metrics import confusion_matrix

from datastructures import Corpus
from read_corpus import read_conll, read_KWDLC
from create_pipeline import tag_corpus_spacy
from visu_matrice_confusion import visu

import time

###### Argparse
def arguments() -> argparse.ArgumentParser:
    """
    Initialise nos arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("corpus", choices=['gsd', 'kwdlc'], help= "type de corpus pour le test")
    return parser.parse_args()

####### Calcul de la précision
def compute_accuracy(
    gold_list: Corpus,
    test_list: Corpus,
    subcorpus: Optional[str] = None,
    confusion: Optional[bool] = False,
    model_name: Optional[str] = None
) -> Tuple[float, float]:
    """
    Permet de calculer l'exactitude d'annotation d'un corpus de test
    par rapport à un corpus de référence
    Peut être fait par sous-corpus
    Peut sortir une matrice de confusion
    """
    correct = 0
    total = 0
    oov_total = 0
    oov_ok = 0

    if confusion:
        y_gold = []
        y_pred = []

    for gold_sentence, test_sentence in zip(gold_list.sents, test_list.sents):
        if subcorpus is None or subcorpus in gold_sentence.sent_id:
            for gold_token, test_token in zip(
                gold_sentence.tokens, test_sentence.tokens
            ):
                assert gold_token.form == test_token.form
                if confusion:
                    y_gold.append(gold_token.tag)
                    y_pred.append(test_token.tag)
                total += 1
                if gold_token.tag == test_token.tag:
                    correct += 1
                if gold_token.is_oov:
                    oov_total += 1
                    if gold_token.tag == test_token.tag:
                        oov_ok += 1

    if confusion:
        visu(y_gold, y_pred, model_name )

    if oov_total == 0 : 
        result_oov = 0 
    else : 
        result_oov = oov_ok / oov_total

    return correct / total, result_oov

def show(gold : Corpus, test : Corpus, model : str,sub : Optional[str] = None) -> str: 

    if sub : 
        subcorp = set()
        for sub in test.sents:
            subcorp.add(sub.sent_id)
            #print(subcorp)

        for sub in subcorp:
            accur, accur_oov = compute_accuracy(gold, test, sub)

        print(
            f"------ {sub} ------\
            \nPrécision du pos tagging : {round(accur, 2)}\
            \nPrécision sur OOV : {round(accur_oov, 2)}"
        )
    
    else: 
        print(f"------ Résultats du modèle {model} ------")
        accur, accur_oov = compute_accuracy(gold, test, confusion=True, model_name=model)
        print(
            f"\n\nPrécision du pos tagging : {round(accur, 2)}\
            \nPrécision sur OOV : {round(accur_oov, 2)}\n\n\n"
        )


if __name__ == "__main__":
    #### lecture de notre corpus conll
    args = arguments()

    if args.corpus == "gsd" : 
        start_1 = time.time()
        corpus_ref, vocab_ref = read_conll("./data/GSD/ja_gsd-ud-train.conllu")
        corpus_gold, vocab_gold = read_conll("./data/GSD/ja_gsd-ud-test.conllu", vocab_ref)

        timer = time.time() - start_1 
        print(f"Lecture du corpus GSD : {round(timer, 2)} secondes ")
        start = time.time()

        spacy_model = spacy.load("ja_core_news_sm")
        corpus_test = tag_corpus_spacy(corpus_gold, spacy_model)

        training = time.time()-start
        print(f"Entraînement du corpus GSD sur ja_core_news_sm: {round(training,2)} secondes")
        
        show(corpus_gold, corpus_test, "Corpus GSD -ja_core_news_sm")
    
        start = time.time()



        trained_model = spacy.load("ja_gsd")
        trained_test = tag_corpus_spacy(corpus_gold, trained_model)
        
        training = time.time()-start
        print(f"Entraînement du corpus GSD sur ja_gsd-1.0: {round(training, 2)} secondes")

        show(corpus_gold, trained_test, "Corpus GSD -ja_gsd-1.0")

        print(f"Temps total : {round(time.time() - start_1, 2)} secondes")

    if args.corpus == "kwdlc" : 
        start_1 = time.time()
        corpus_ref, vocab_ref= read_KWDLC("./data/KWDLC/train.id")
        corpus_gold, vocab_gold = read_KWDLC("./data/KWDLC/test.id", vocab_ref)  
        
        timer = time.time() - start_1 
        print(f"Lecture du corpus KWDLC : {round(timer, 2)} secondes ")

        start = time.time()
        spacy_model = spacy.load("ja_core_news_sm")
        corpus_test = tag_corpus_spacy(corpus_gold, spacy_model)
        
        training = time.time()-start
        print(f"Entraînement du corpus KWDLC sur ja_core_news_sm: {round(training,2)} secondes")

        show(corpus_gold, corpus_test, "Corpus KWDLC - ja_core_news_sm")

        start = time.time()

        trained_model = spacy.load("ja_gsd")
        trained_test = tag_corpus_spacy(corpus_gold, trained_model)

        training = time.time()-start
        print(f"Entraînement du corpus KWDLC sur ja_gsd-1.0: {round(training, 2)} secondes")

        show(corpus_gold, trained_test, "Corpus KWDLC - ja_gsd-1.0")

        print(f"Temps total : {round(time.time() - start_1, 2)} secondes")
    
