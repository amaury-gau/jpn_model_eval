from dataclasses import dataclass 
from typing import List

@dataclass 
class Token:
    form: str
    tag: str
    is_oov: bool

@dataclass
class Sent:
    sent_id : str
    tokens: List[Token]

@dataclass
class Corpus:
    sents: List[Sent]