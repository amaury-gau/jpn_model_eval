import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix

from typing import List


def visu(y_gold: List[str],
         y_pred: List[str], 
         model_name: str) -> plt:
    
    labels = list(set(y_gold))

    confusion = confusion_matrix(y_gold, y_pred)
    sns.heatmap(confusion, annot=True, fmt='g', cmap='Blues',
                xticklabels=labels, yticklabels=labels)
    plt.xlabel('Classe Prédite')
    plt.ylabel('Classe Réelle')
    plt.title(f'Matrice de Confusion : {model_name}')
    plt.show()  