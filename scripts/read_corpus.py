import re
import os
from datastructures import Corpus, Sent, Token

from pathlib import Path
from typing import List, Tuple, Set, Optional

import glob


##### Read Conll
def read_conll(path: Path, vocab: Optional[Set[str]] = None) -> Tuple[Corpus, Set[str]]:
    """
    Lit un fichier conll et le formate dans une classe corpus
    path : lien Path vers le fichier conll 
    vocab : un dictionnaire de vocabulaire 
    renvoie une valeur de classe Corpus 
    """

    sentences: List[Sent] = []
    tokens: List[Token] = []
    vocabulary = set()
    with open(path, "r", encoding="utf-8") as f:
        for line in f:
            line = line.strip()
            if line.startswith("# sent_id"):
                # On récupère le nom
                sent_id = re.findall(r"# sent_id = (.+)", line)
                #print(sent_id[0])
            if not line.startswith("#"):
                if line == "":
                    sentences.append(Sent(sent_id[0], tokens))
                    tokens = []
                else:
                    fields = line.split("\t")
                    form, pos = fields[1], fields[3]
                    vocabulary.add(form)
                    if not "-" in fields[0]:  # éviter les contractions type "du"
                        if vocab is None:
                            is_oov = True
                        else:
                            is_oov = not form in vocab
                        tokens.append(Token(form, pos, is_oov))
    return Corpus(sentences), vocabulary

def read_KWDLC(path: str, vocab: Optional[Set[str]] = None) -> Corpus:

    MAP = {
    '特殊': 'PUNCT',
    '副詞': 'ADV',
    '動詞': 'VERB',
    '接続詞': 'CONJ',
    '感動詞': 'INTJ',
    '助詞': 'ADP',  
    '助動詞': 'AUX',
    '判定詞': 'AUX', 
    '指示詞': 'PRON', 
    '未定義語': 'X',  #ça c'est OOV, mais je sais pas trop, ça existe pas dans Spacy
    '接尾辞': 'SUFFIX',
    '形容詞': 'ADJ',
    '連体詞': 'ADJ',  
    '名詞': 'NOUN',
    '接頭辞': 'PREFIX',
    }

    with open(path, 'r') as fin:
        list_file_name = fin.readlines()
    list_file_name = [file.strip()+'.knp' for file in list_file_name]
    sentences: List[Sent] = []
    tokens: List[Token] = []
    vocabulaire = set()
    for file in glob.glob("./data/KWDLC/knp/*/*"):
        filename = os.path.basename(file)
        if filename in list_file_name:
            with open(file, 'r') as f:
                for line in f:
                    line = line.strip()
                    if line.startswith("# "):
                        phrase_id = line.split()[1].split(':')[1]
                    elif not line.startswith("*") and not line.startswith("+"):
                        if line == "EOS":
                            sentences.append(Sent(sent_id=phrase_id, tokens=tokens))
        # #                    # if phrase_id is not None:
        # #                     #     sentences.append(Sentence(sent_id=phrase_id, tokens=tokens))
        # #                     # else:
        # #                     #     sentences.append(Sentence(tokens=tokens))

                            tokens = []
                            phrase_id = None
                        else:
                            fields = line.split()
                            form, tag = fields[0], fields[3]
                            tag = MAP.get(tag)
                            vocabulaire.add(form)
                            if not "-" in fields[0]:  # éviter les contractions type "du"
                                if vocab is None:
                                    is_oov = True
                                else:
                                    is_oov = not form in vocabulaire # même chose que False if form in vocabulaire
                                tokens.append(Token(form, tag, is_oov))

    return Corpus(sentences), vocabulaire

if __name__ == '__main__':

    import sys 
    doesitwork, vocab=read_conll(sys.argv[1])
    print(doesitwork)