from datastructures import Corpus, Sent, Token 
from typing import List, Optional

from spacy import Language as SpacyPipeline
from spacy.tokens import Doc as SpacyDoc

import time 

####### Création des corpus de test

def sentence_to_doc(sentence: List[Token], vocab) -> SpacyDoc:
    """
    transforme une liste de tokens en classe SpacyDoc
    """
    words = [tok.form for tok in sentence]
    return SpacyDoc(vocab, words=words)


def doc_to_sentence(sent_id: str, doc: SpacyDoc, origin: Sent) -> Sent:
    """
    Transforme des classes SpacyDoc en classe Sent
    """
    tokens = []
    for tok, origin_tok in zip(doc, origin.tokens):
        tokens.append(Token(tok.text, tok.pos_, is_oov=origin_tok.is_oov))
    return Sent(sent_id, tokens)


def tag_corpus_spacy(corpus: Corpus, model_spacy: SpacyPipeline) -> Corpus:
    """
    Transforme l'annotation de spacy en classe Corpus pour comparer avec
    les fichiers conll
    """
    sentences = []

    # print([sent for sent in corpus.sents])
    for sentence in corpus.sents:
        doc = sentence_to_doc(sentence.tokens, model_spacy.vocab)
        new_doc = model_spacy(doc)   
        sentences.append(doc_to_sentence(sentence.sent_id, new_doc, sentence))
    return Corpus(sentences)
