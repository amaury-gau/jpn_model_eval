#!/bin/python3
# -*- coding: utf-8 -*-

""" Script pour évaluer la performance du POS-tagging du module Nagisa
sur des données UD (conll) et des des données knp (corpus KWDLC)

Exécution : 
python3 eval_nagisa.py
"""

import os
from typing import Any, List, Set, Optional, Tuple
from pathlib import Path
import glob
from dataclasses import dataclass

import nagisa


######################
# DATACLASS
######################
@dataclass
class Token:
    form: str
    tag: str
    is_oov: bool


@dataclass
class Sentence:
    sent_id: str
    tokens: List[Token]


@dataclass
class Corpus:
    sentences: List[Sentence]


########################
# création du corpus
########################
def read_conll(path: Path, vocabulaire: Optional[Set[str]] = None) -> Corpus:
    """Lit un fichier .conll et créer un objet corpus contenant les phrases contenu 
    dans le fichier. Les phrases contiennent les tokens qui ont une forme et un postag
    """
    sentences = []
    tokens: List[Token] = []
    with open(path) as f:
        for line in f:
            line = line.strip()
            if line.startswith("# sent_id"):
                phrase_id = line.split()[-1]
            elif not line.startswith("#"):
                if line == "":
                    sentences.append(Sentence(sent_id=phrase_id, tokens=tokens))
                    tokens = []
                    phrase_id = None
                else:
                    fields = line.split("\t")
                    form, tag = fields[1], fields[4].split('-')[0]
                    if not "-" in fields[0]:  # éviter les contractions type "du"
                        if vocabulaire is None:
                            is_oov = True
                        else:
                            is_oov = (
                                not form in vocabulaire
                            )  # même chose que False if form in vocabulaire
                        tokens.append(Token(form, tag, is_oov))

    return Corpus(sentences)

def read_KWDLC(type_corpus: str, vocabulaire: Optional[Set[str]] = None) -> Corpus:
    """Créer un objet corpus à partir de fichiers .knp. Les fichiers .knp sont sélectionnés
    selon une liste de noms de fichier associée au type du corpus

    Args:
         - type_corpus: "train", "dev" ou "test"
    """
    with open('../data/KWDLC/'+type_corpus+'.id', 'r') as fin:
        list_file_name = fin.readlines()
    list_file_name = [file.strip()+'.knp' for file in list_file_name]
    sentences = []
    tokens: List[Token] = []
    for file in glob.glob("../data/KWDLC/knp/*/*"):
        filename = os.path.basename(file)
        if filename in list_file_name:
            with open(file, 'r') as f:
                for line in f:
                    line = line.strip()
                    if line.startswith("# "):
                        phrase_id = line.split()[1].split(':')[1]
                    elif not line.startswith("*") and not line.startswith("+"):
                        if line == "EOS":
                            sentences.append(Sentence(sent_id=phrase_id, tokens=tokens))

                            tokens = []
                            phrase_id = None
                        else:
                            fields = line.split()
                            form, tag = fields[0], fields[3]
                            if not "-" in fields[0]:  # éviter les contractions type "du"
                                if vocabulaire is None:
                                    is_oov = True
                                else:
                                    is_oov = (
                                        not form in vocabulaire
                                    )  # même chose que False if form in vocabulaire
                                tokens.append(Token(form, tag, is_oov))

    return Corpus(sentences)


#########################
# extraction vocabulaire
#########################
def vocab_extrac(path: Path) -> set:
    """Extraction du vocabulaire du fichier fournit en entrée (pas utile ici)"""
    vocabulaire = set()
    corpus_train = read_conll(path)
    for sentence in corpus_train.sentences:
        for token in sentence.tokens:
            vocabulaire.add(token.form)
    return vocabulaire

##################################
# tag d'un Corpus avec nagisa
##################################
def tag_corpus_nagisa(corpus: Corpus, tagger=None) -> Corpus:
    """
    Transforme l'annotation de nagisa en classe Corpus
    """
    sentences = []
    for sentence in corpus.sentences:
        tokens = []
        sentence_str = "".join([token.form for token in sentence.tokens])
        # print(sentence_str)
        if tagger is None:
            words = nagisa.tagging(sentence_str)
            # print(words)
            for word, tag in zip(words.words, words.postags):
                tokens.append(Token(word, tag, is_oov=False))
        else:
            words = tagger.tagging(sentence_str)
            # print(words)
            for word, tag in zip(words.words, words.postags):
                tokens.append(Token(word, tag.split('-')[0], is_oov=False))
        
        sentences.append(Sentence(sentence.sent_id, tokens))
        # print(sentences)
    return Corpus(sentences)


#########################################
# calcul de l'accuracy entre deux corpus
#########################################
def compute_accuracy(
    gold_list: Corpus,
    test_list: Corpus,
    subcorpus: Optional[str] = None,
    confusion: Optional[bool] = False,
) -> Tuple[float, float]:
    """
    Permet de calculer l'exactitude d'annotation d'un corpus de test
    par rapport à un corpus de référence
    Peut être fait par sous-corpus
    Peut sortir une matrice de confusion
    """
    correct = 0
    total = 0
    oov_total = 0
    oov_ok = 0

    if confusion:
        y_gold = []
        y_pred = []

    for gold_sentence, test_sentence in zip(gold_list.sentences, test_list.sentences):
        if subcorpus is None or subcorpus in gold_sentence.sent_id:
            for gold_token, test_token in zip(
                gold_sentence.tokens, test_sentence.tokens
            ):
                # assert gold_token.form == test_token.form
                if confusion:
                    y_gold.append(gold_token.tag)
                    y_pred.append(test_token.tag)
                total += 1
                if gold_token.tag == test_token.tag:
                    correct += 1
                if gold_token.is_oov:
                    oov_total += 1
                    if gold_token.tag == test_token.tag:
                        oov_ok += 1

    # je ne peux pas m'en servir à cause de sklearn/scikit-learn qui ne fonctionne pas sur mon ordinateur 
    # if confusion:
    #     visu(y_gold, y_pred, model_name )

    if oov_total == 0 : 
        result_oov = 0 
    else : 
        result_oov = oov_ok / oov_total

    return correct / total, result_oov


######################################
# confusion matrix simple sur les POS
######################################
def show(gold : Corpus, test : Corpus, model : str,sub : Optional[str] = None) -> str: 
    """Affiche les résultats"""
    if sub : 
        subcorp = set()
        for sub in test.sents:
            subcorp.add(sub.sent_id)
            #print(subcorp)

        for sub in subcorp:
            accur, accur_oov = compute_accuracy(gold, test, sub)

        print(
            f"------ {sub} ------\
            \nPrécision du pos tagging : {round(accur, 2)}\
            \nPrécision sur OOV : {round(accur_oov, 2)}"
        )
    
    else: 
        print(f"------ Résultats du modèle {model} ------")
        accur, accur_oov = compute_accuracy(gold, test, confusion=True)
        print(
            f"\n\nPrécision du pos tagging : {round(accur, 2)}\
            \nPrécision sur OOV : {round(accur_oov, 2)}\n\n\n"
        )

# Ne peut pas m'en servir pour l'instant à cause de sklearn
# def make_confusion_matrix(corpus_gold: Corpus, corpus_test: Corpus) -> tuple:
#     tag_true = []
#     tag_pred = []
#     for sentence_gold, sentence_test in zip(
#         corpus_gold.sentences, corpus_test.sentences
#     ):
#         for token_gold, token_test in zip(sentence_gold.tokens, sentence_test.tokens):
#             tag_true.append(token_gold.tag)
#             tag_pred.append(token_test.tag)
#     set_unique_tag = set(tag_true)
#     return confusion_matrix(tag_true, tag_pred), set_unique_tag


#########################################
# sauvegarde SVG de la confusion matrix
#########################################
# def beautiful_confusion_matrix(confusion_matrix: Any, set_unique_tag: Set) -> None:
#     # création d'une figure et d'un axe pour le graphique
#     # f, ax = plt.subplots(figsize=(8,6))

#     # création d'une heatmap pour visualiser la matrice de confusion
#     sns.heatmap(confusion_matrix, annot=True, cmap="Blues", robust=True, linewidth=1, xticklabels=set_unique_tag, yticklabels=set_unique_tag)

#     # titre du graphique et étiquettes des axes
#     plt.title("Matrice de confusion - données de test", fontsize=15, fontweight="bold")
#     plt.xlabel("Etiquette prédite", fontsize=14)
#     plt.ylabel("Etiquette correcte", fontsize=14)
    
#     # sauvegarde matrice
#     plt.savefig("confusion_matrix.svg")


#######
# MAIN
#######
def main():
    gsd_tagger = nagisa.Tagger(vocabs='../models/nagisa/ja_gsd_ud_jptag.vocabs',
                              params='../models/nagisa/ja_gsd_ud_jptag.params',
                              hp='../models/nagisa/ja_gsd_ud_jptag.hp')
    kwdlc_tagger = nagisa.Tagger(vocabs='../models/nagisa/ja_kwdlc_knp.vocabs',
                              params='../models/nagisa/ja_kwdlc_knp.params',
                              hp='../models/nagisa/ja_kwdlc_knp.hp')
    
    # Chargement des deux corpus GSD et KWDLC
    corpus_gold_gsd = read_conll(Path("../data/GSD/ja_gsd-ud-test.conllu"))
    corpus_gold_kwdlc = read_KWDLC("test")

    for corpus_gold, name_gold in zip((corpus_gold_gsd, corpus_gold_kwdlc), ("gsd", "kwdlc")):
        for tagger, name_tagger in zip((None, gsd_tagger, kwdlc_tagger), ("tbaseline", "tgsd", "tkwdlc")):
            corpus_test = tag_corpus_nagisa(corpus_gold, tagger)
            # print(f"Score global ({name_gold}, {name_tagger}): {compute_accuracy(corpus_gold, corpus_test)}")
            show(corpus_gold, corpus_test, f"Corpus {name_gold} - {name_tagger}")

    # cm, unique_tag = make_confusion_matrix(corpus_gold=corpus_gold, corpus_test=corpus_test)
    # beautiful_confusion_matrix(cm, unique_tag)

if __name__ == "__main__":
    main()
