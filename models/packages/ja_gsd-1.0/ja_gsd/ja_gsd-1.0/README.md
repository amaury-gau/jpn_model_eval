| Feature | Description |
| --- | --- |
| **Name** | `ja_gsd` |
| **Version** | `1.0` |
| **spaCy** | `>=3.7.2,<3.8.0` |
| **Default Pipeline** | `tok2vec`, `morphologizer`, `parser` |
| **Components** | `tok2vec`, `morphologizer`, `parser` |
| **Vectors** | 0 keys, 0 unique vectors (0 dimensions) |
| **Sources** | n/a |
| **License** | n/a |
| **Author** | [n/a]() |

### Label Scheme

<details>

<summary>View label scheme (42 labels for 2 components)</summary>

| Component | Labels |
| --- | --- |
| **`morphologizer`** | `POS=NOUN`, `POS=ADP`, `POS=VERB`, `POS=SCONJ`, `POS=AUX`, `POS=PUNCT`, `POS=PART`, `POS=DET`, `POS=NUM`, `POS=ADV`, `POS=PRON`, `POS=ADJ`, `POS=PROPN`, `POS=CCONJ`, `POS=SYM`, `POS=NOUN\|Polarity=Neg`, `POS=AUX\|Polarity=Neg`, `POS=INTJ`, `POS=SCONJ\|Polarity=Neg` |
| **`parser`** | `ROOT`, `acl`, `advcl`, `advmod`, `amod`, `aux`, `case`, `cc`, `ccomp`, `compound`, `cop`, `csubj`, `dep`, `det`, `fixed`, `mark`, `nmod`, `nsubj`, `nsubj:outer`, `nummod`, `obj`, `obl`, `punct` |

</details>

### Accuracy

| Type | Score |
| --- | --- |
| `POS_ACC` | 96.80 |
| `MORPH_MICRO_F` | 50.51 |
| `DEP_UAS` | 91.21 |
| `DEP_LAS` | 89.20 |
| `SENTS_P` | 95.86 |
| `SENTS_R` | 93.92 |
| `SENTS_F` | 94.88 |
| `TAG_ACC` | 0.00 |
| `TOK2VEC_LOSS` | 2397022.93 |
| `MORPHOLOGIZER_LOSS` | 417887.11 |
| `PARSER_LOSS` | 3432265.81 |