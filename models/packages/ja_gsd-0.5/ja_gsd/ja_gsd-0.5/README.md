| Feature | Description |
| --- | --- |
| **Name** | `ja_gsd` |
| **Version** | `0.5` |
| **spaCy** | `>=3.7.2,<3.8.0` |
| **Default Pipeline** | `tok2vec`, `morphologizer`, `parser` |
| **Components** | `tok2vec`, `morphologizer`, `parser` |
| **Vectors** | 0 keys, 0 unique vectors (0 dimensions) |
| **Sources** | n/a |
| **License** | n/a |
| **Author** | [n/a]() |

### Label Scheme

<details>

<summary>View label scheme (42 labels for 2 components)</summary>

| Component | Labels |
| --- | --- |
| **`morphologizer`** | `POS=NOUN`, `POS=ADP`, `POS=VERB`, `POS=SCONJ`, `POS=AUX`, `POS=PUNCT`, `POS=PART`, `POS=DET`, `POS=NUM`, `POS=ADV`, `POS=PRON`, `POS=ADJ`, `POS=PROPN`, `POS=CCONJ`, `POS=SYM`, `POS=NOUN\|Polarity=Neg`, `POS=AUX\|Polarity=Neg`, `POS=INTJ`, `POS=SCONJ\|Polarity=Neg` |
| **`parser`** | `ROOT`, `acl`, `advcl`, `advmod`, `amod`, `aux`, `case`, `cc`, `ccomp`, `compound`, `cop`, `csubj`, `dep`, `det`, `fixed`, `mark`, `nmod`, `nsubj`, `nsubj:outer`, `nummod`, `obj`, `obl`, `punct` |

</details>

### Accuracy

| Type | Score |
| --- | --- |
| `POS_ACC` | 96.39 |
| `MORPH_MICRO_F` | 50.51 |
| `DEP_UAS` | 89.96 |
| `DEP_LAS` | 87.35 |
| `SENTS_P` | 95.11 |
| `SENTS_R` | 93.19 |
| `SENTS_F` | 94.14 |
| `TAG_ACC` | 0.00 |
| `TOK2VEC_LOSS` | 495102.76 |
| `MORPHOLOGIZER_LOSS` | 138618.04 |
| `PARSER_LOSS` | 841160.13 |